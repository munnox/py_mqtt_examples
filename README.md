# A simple MQTT message example

For debugging and testing. Currently uses Mosquitto Mqtt docker server.


Further info: http://www.steves-internet-guide.com/into-mqtt-python-client/

Paho examples: https://github.com/eclipse/paho.mqtt.python/tree/master/examples

```
docker run -it -p 1883:1883 -p 9001:9001 eclipse-mosquitto
```

Full command to run is:

```
docker run -it -p 1883:1883 -p 9001:9001 -v mosquitto.conf:/mosquitto/config/mosquitto.conf -v /mosquitto/data -v /mosquitto/log eclipse-mosquitto
```

This requires the configuration file

To the run the examples:

```
python3 -m venv pyvenv
source pyvenv/bin/activate
pip install -r requirements.txt
```

Then in the enviroment run

```
python sub.py
```

and 

```
python pub.py
```

Keep an eye on the stdout as `pub.py` passes message into the queue and `sub.py` pull them out and displays them on a call back.
