"""
Subscription mqtt debug example

Further info: http://www.steves-internet-guide.com/into-mqtt-python-client/
Paho examples: https://github.com/eclipse/paho.mqtt.python/tree/master/examples

"""

import paho.mqtt.client as mqtt
import datetime
import time

client_name = "pub1"
client = mqtt.Client(client_name)

host_name = "127.0.0.1"
client.connect(host_name, port=1883, keepalive=60, bind_address="")

topic = "test/switch"

while True:
    payload = datetime.datetime.now().isoformat()
    result = client.publish(topic, payload=payload, qos=0, retain=False)
    time.sleep(1)
    print(f"Publish: {topic}, {payload}, {result}")
