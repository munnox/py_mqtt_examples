"""
Subscription mqtt debug example

Further info: http://www.steves-internet-guide.com/into-mqtt-python-client/
Paho examples: https://github.com/eclipse/paho.mqtt.python/tree/master/examples

"""

import paho.mqtt.client as mqtt
import datetime
import time

client_name = "pub2"
client = mqtt.Client(client_name)

host_name = "127.0.0.1"
client.connect(host_name, port=1883, keepalive=60, bind_address="")

topic = "test/switch"

info = client.subscribe(topic, qos=0)
print(f"Subscribe: {topic} {info}")

def on_message(client, userdata, message):
    print(f"Message recieved: {message.topic} {str(message.payload.decode('utf-8'))}")

client.on_message = on_message

# while True

client.loop_start()
try:
    while True:
        time.sleep(0.5)
except Exception as error:
    print(f"Exit on Exception {error}")
finally:
    client.loop_stop()

client.disconnect()
